package com.yash.walletapp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yash.walletapp.R;
import com.yash.walletapp.activity.AcceptMyWalletActivity;
import com.yash.walletapp.activity.AddMoneyActivity;
import com.yash.walletapp.activity.BaseActivity;
import com.yash.walletapp.activity.HomeActivity;
import com.yash.walletapp.activity.NotificationActivity;
import com.yash.walletapp.activity.PassBookActivity;
import com.yash.walletapp.activity.PayActivity;
import com.yash.walletapp.utils.ConnectionDetector;
import com.yash.walletapp.utils.WalletAppConstants;
import com.yash.walletapp.utils.WalletAppDialogs;


public class DrawerFragment extends Fragment {

    public ActionBarDrawerToggle mDrawerToggle;
    private int selectedPos = -1;
    private DrawerLayout drawerlayout;
    private TextView notification_title, download_title;
    private TextView logout_title, userName, myAcc, cart_count_drawer;
    private ImageView userImage;
    private LinearLayout pay, name_text_layout, dear_user_text_layout;

    private ConnectionDetector cd;
    private LinearLayout logout;

    public DrawerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.left_slide_list_layout, container, false);

        selectedPos = -1;

        cd = new ConnectionDetector(getActivity());

        dear_user_text_layout = (LinearLayout) v.findViewById(R.id.dear_user_text_layout);
        name_text_layout = (LinearLayout) v.findViewById(R.id.name_text_layout);
        final LinearLayout home = (LinearLayout) v.findViewById(R.id.home_menu_option);
        final LinearLayout addMoney = (LinearLayout) v.findViewById(R.id.add_money_menu_option);
        pay = (LinearLayout) v.findViewById(R.id.pay_menu_option);
        final LinearLayout passbook = (LinearLayout) v.findViewById(R.id.passbook_menu_option);
        final LinearLayout accept = (LinearLayout) v.findViewById(R.id.accept_menu_option);
        final LinearLayout notification = (LinearLayout) v.findViewById(R.id.notification_menu_option);
        logout = (LinearLayout) v.findViewById(R.id.logout_option);

        setClickListener(home, 0);
        setClickListener(addMoney, 1);
        setClickListener(pay, 2);
        setClickListener(passbook, 3);
        setClickListener(accept, 4);
        setClickListener(notification, 5);
        setClickListener(logout, 6);

        ImageView home_icon = (ImageView) v.findViewById(R.id.home_icon);
        ImageView add_money_icon = (ImageView) v.findViewById(R.id.add_money_icon);
        ImageView pay_icon = (ImageView) v.findViewById(R.id.pay_icon);
        ImageView passbook_icon = (ImageView) v.findViewById(R.id.passbook_icon);
        ImageView accept_icon = (ImageView) v.findViewById(R.id.accept_icon);
        ImageView notification_icon = (ImageView) v.findViewById(R.id.notification_icon);
        ImageView logout_icon = (ImageView) v.findViewById(R.id.logout_icon);

        TextView home_title = (TextView) v.findViewById(R.id.home_title);
        TextView add_money_title = (TextView) v.findViewById(R.id.add_money_title);
        TextView pay_title = (TextView) v.findViewById(R.id.pay_title);
        TextView passbook_title = (TextView) v.findViewById(R.id.passbook_title);
        TextView accept_title = (TextView) v.findViewById(R.id.accept_title);
        TextView notification_title = (TextView) v.findViewById(R.id.notification_title);
        TextView logout_title = (TextView) v.findViewById(R.id.logout_title);


        userName = (TextView) v.findViewById(R.id.home_menu_UserName);
        myAcc = (TextView) v.findViewById(R.id.home_menu_MyAcc);
        userImage = (ImageView) v.findViewById(R.id.home_menu_UserImage);

        if (getFromPrefs(WalletAppConstants.EMAIL) != null && !getFromPrefs(WalletAppConstants.EMAIL).equals("")) {
            logout.setVisibility(View.VISIBLE);
            logout_title.setText("Logout");
            userName.setText(getFromPrefs(WalletAppConstants.EMAIL));
            myAcc.setText("");
            if (getFromPrefs("image") != null && !getFromPrefs("image").equals(""))
                ((BaseActivity) getActivity()).setProfileImageInLayout(getActivity(), (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image),
                        getFromPrefs("image"), userImage);
        } else {
            logout.setVisibility(View.GONE);
        }

        dear_user_text_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        myAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return v;
    }

    private void setClickListener(final LinearLayout layout, final int pos) {
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        selectedPos = pos;
                        drawerlayout.closeDrawers();
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        selectedPos = -1;
                        break;
                }
                return true;
            }
        });
    }

    @SuppressLint("NewApi")
    public void setUp(final DrawerLayout drawerlayout) {

        this.drawerlayout = drawerlayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerlayout, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
                ((BaseActivity) getActivity()).hideSoftKeyboard();
                if (getFromPrefs(WalletAppConstants.EMAIL) != null && !getFromPrefs(WalletAppConstants.EMAIL).equals("")) {
                    logout.setVisibility(View.VISIBLE);
                    userName.setText(getFromPrefs(WalletAppConstants.EMAIL));
                    myAcc.setText("");
                    if (getFromPrefs("image") != null && !getFromPrefs("image").equals(""))
                        ((BaseActivity) getActivity()).setProfileImageInLayout(getActivity(), (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image),
                                getFromPrefs("image"), userImage);
                } else {
                    logout.setVisibility(View.GONE);
                }
            }

            @SuppressLint("NewApi")
            @Override
            public void onDrawerClosed(View drawerView) {
                ActivityManager am = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                super.onDrawerClosed(drawerView);
                if (selectedPos != -1) {
                    switch (selectedPos) {
                        case 0:
                            if (!cn.getShortClassName().equals(".activity.HomeActivity")) {
                                navigateToHome();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 1:
                            if (!cn.getShortClassName().equals(".activity.AddMoneyActivity")) {
                                navigateToAddMoney();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 2:
                            if (!cn.getShortClassName().equals(".activity.PayActivity")) {

                                    navigateToPay();

                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 3:
                            if (!cn.getShortClassName().equals(".activity.PassBookActivity")) {
                                navigateToPassbook();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 4:
                            if (!cn.getShortClassName().equals(".activity.AcceptMyWalletActivity")) {

                                    navigateToAcceptMyWallet();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 5:
                            if (!cn.getShortClassName().equals(".activity.NotificationActivity")) {
                                navigateToNotifications();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;

                        case 6:
                            if (!cn.getShortClassName().equals(".activity.BlogActivity")) {
                                logout();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;


                        default:
                            break;
                    }
                    selectedPos = -1;
                }
            }
        };

        drawerlayout.setDrawerListener(mDrawerToggle);

    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getActivity().getSharedPreferences(WalletAppConstants.PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, WalletAppConstants.DEFAULT_VALUE);
    }

    private void navigateToHome() {
        drawerlayout.closeDrawers();
        for (int i = 0; i < WalletAppConstants.ACTIVITIES.size(); i++) {
            if (WalletAppConstants.ACTIVITIES.get(i) != null)
                WalletAppConstants.ACTIVITIES.get(i).finish();
        }
        getActivity().finish();
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private void navigateToAddMoney() {
        drawerlayout.closeDrawers();
        Intent intent = new Intent(getActivity(), AddMoneyActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private void navigateToPay() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".PayActivity");
        Intent intent = new Intent(getActivity(), PayActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private void navigateToPassbook() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".PassBookActivity");
        Intent intent = new Intent(getActivity(), PassBookActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);

    }

    private void navigateToNotifications() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".NotificationsActivity");
        Intent intent = new Intent(getActivity(), NotificationActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);

    }

    private void navigateToAcceptMyWallet() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".AcceptMyWalletActivity");
        Intent intent = new Intent(getActivity(), AcceptMyWalletActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }


    private void logout() {
        WalletAppDialogs logout_dialogs = new WalletAppDialogs(getActivity());
        logout_dialogs.displayDialogWithCancel("Are you sure you want to Logout");
    }

    private void removeActivity(String activity) {
        for (int i = 0; i < WalletAppConstants.ACTIVITIES.size(); i++) {
            if (WalletAppConstants.ACTIVITIES.get(i) != null && WalletAppConstants.ACTIVITIES.get(i).toString().contains(activity)) {
                WalletAppConstants.ACTIVITIES.get(i).finish();
                WalletAppConstants.ACTIVITIES.remove(i);
                break;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

}
