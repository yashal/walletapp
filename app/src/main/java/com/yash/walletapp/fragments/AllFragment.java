package com.yash.walletapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yash.walletapp.R;
import com.yash.walletapp.activity.PassBookActivity;
import com.yash.walletapp.adapter.AllPassbookAdapter;
import com.yash.walletapp.adapter.NotificationAdapter;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class AllFragment extends Fragment {

    private View view;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    //    private ArrayList<PgComplaintDataPojo> complaintList;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_all, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.all_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter = new AllPassbookAdapter((PassBookActivity)getActivity());
        recyclerView.setAdapter(mAdapter);

        return view;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

    }
}