package com.yash.walletapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yash.walletapp.R;
import com.yash.walletapp.activity.NotificationActivity;


/**
 * Created by quepplin1 on 2/9/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private int height, width;

    private NotificationActivity context;

    public NotificationAdapter(NotificationActivity context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.itemView.setOnClickListener(context);
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView notification_title_text, notification_message_text, notification_date_text;

        public ViewHolder(View itemView) {
            super(itemView);
            notification_title_text = (TextView) itemView.findViewById(R.id.notification_title_text);
            notification_message_text = (TextView) itemView.findViewById(R.id.notification_message_text);
            notification_date_text = (TextView) itemView.findViewById(R.id.notification_date_text);

        }
    }
}
