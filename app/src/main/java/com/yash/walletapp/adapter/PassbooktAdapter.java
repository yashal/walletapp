package com.yash.walletapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.yash.walletapp.fragments.AllFragment;
import com.yash.walletapp.fragments.PaiedFragment;
import com.yash.walletapp.fragments.ReceivedFragment;
import com.yash.walletapp.fragments.SentFragment;


/**
 * Created by Yash ji on 4/22/2016.
 */
public class PassbooktAdapter extends FragmentPagerAdapter {
    public PassbooktAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new AllFragment();
            case 1:
                return new PaiedFragment();
            case 2:
                return new ReceivedFragment();
            case 3:
                return new SentFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }
}

