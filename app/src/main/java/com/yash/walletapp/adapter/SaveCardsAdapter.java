package com.yash.walletapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yash.walletapp.R;
import com.yash.walletapp.activity.SaveCardsActivity;


/**
 * Created by quepplin1 on 2/9/2016.
 */
public class SaveCardsAdapter extends RecyclerView.Adapter<SaveCardsAdapter.ViewHolder> {

    private int height, width;

    private SaveCardsActivity context;

    public SaveCardsAdapter(SaveCardsActivity context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.save_cards_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.itemView.setOnClickListener(context);
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView bank_name, card_number;

        public ViewHolder(View itemView) {
            super(itemView);
            bank_name = (TextView) itemView.findViewById(R.id.bank_name);
            card_number = (TextView) itemView.findViewById(R.id.card_number);

        }
    }
}
