package com.yash.walletapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yash.walletapp.R;
import com.yash.walletapp.activity.NotificationActivity;
import com.yash.walletapp.activity.PassBookActivity;


/**
 * Created by quepplin1 on 2/9/2016.
 */
public class AllPassbookAdapter extends RecyclerView.Adapter<AllPassbookAdapter.ViewHolder> {

    private int height, width;

    private PassBookActivity context;

    public AllPassbookAdapter(PassBookActivity context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.itemView.setOnClickListener(context);
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView passbook_title_text,passbook_amount_text,  passbook_receiver_text, passbook_date_text;

        public ViewHolder(View itemView) {
            super(itemView);
            passbook_title_text = (TextView) itemView.findViewById(R.id.passbook_title_text);
            passbook_amount_text = (TextView) itemView.findViewById(R.id.passbook_amount_text);
            passbook_receiver_text = (TextView) itemView.findViewById(R.id.passbook_receiver_text);
            passbook_date_text = (TextView) itemView.findViewById(R.id.passbook_date_text);

        }
    }
}
