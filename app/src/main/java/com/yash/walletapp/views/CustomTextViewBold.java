package com.yash.walletapp.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by Administrator on 25-Nov-15.
 */
public class CustomTextViewBold extends TextView {
   /* public CustomTextViewBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextViewBold(Context context) {
        super(context);
        init();
    }*/
/*private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Lato-Semibold.ttf");
        setTypeface(tf);
    }*/


    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Semibold.ttf");
        this.setTypeface(face);
//        this.setTypeface(null, Typeface.BOLD);
    }

}
