package com.yash.walletapp.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;


public class CustomEditText extends EditText {
	/*public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public CustomEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CustomEditText(Context context) {
		super(context);
		init();
	}*/

	/*private void init() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Lato-Light.ttf");
		setTypeface(tf);
	}*/

	public CustomEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Light.ttf");
		this.setTypeface(face);
//		this.setTypeface(null);
	}
	

}
