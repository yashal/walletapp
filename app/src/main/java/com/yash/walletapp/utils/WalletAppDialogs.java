package com.yash.walletapp.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yash.walletapp.R;
import com.yash.walletapp.activity.BaseActivity;
import com.yash.walletapp.activity.HomeActivity;
import com.yash.walletapp.activity.LoginActivity;


/**
 * Created by Administrator on 15-Feb-16.
 */
public class WalletAppDialogs {

    private Activity ctx;
    public static final String PREFS_NAME = "MyPrefsFile1";

    public static ProgressDialog showLoading(Activity activity) {
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        if (!activity.isFinishing() && !mProgressDialog.isShowing())
            mProgressDialog.show();
        return mProgressDialog;
    }

    public WalletAppDialogs(Activity ctx) {
        this.ctx = ctx;
    }

    public void displayCommonDialogWithHeaderSmall(String header, String msg) {
        TextView OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog_small);
        if (!DialogLogOut.isShowing()) {
            TextView msg_textView = (TextView) DialogLogOut.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) DialogLogOut.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(msg);
            dialog_header.setText(header);
            OkButtonLogout = (TextView) DialogLogOut.findViewById(R.id.btn_yes_exit);
            ImageView dialog_header_cross = (ImageView) DialogLogOut.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogLogOut.dismiss();
                }
            });

            OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    DialogLogOut.dismiss();
                }
            });
            DialogLogOut.show();
        }
    }

    public void displayDialogWithCancel(String msg) {
        LinearLayout OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog);
        TextView loogout_msg = (TextView) DialogLogOut.findViewById(R.id.text_exit);
        loogout_msg.setText(msg);
        OkButtonLogout = (LinearLayout) DialogLogOut.findViewById(R.id.btn_yes_exit_LL);
        LinearLayout CancelButtonLogout = (LinearLayout) DialogLogOut.findViewById(R.id.btn_no_exit_LL);

        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
                for (int i = 0; i < WalletAppConstants.ACTIVITIES.size(); i++) {
                    if (WalletAppConstants.ACTIVITIES.get(i) != null)
                        WalletAppConstants.ACTIVITIES.get(i).finish();
                }
                ((BaseActivity) ctx).saveIntoPrefs(WalletAppConstants.NAME, "");
                ((BaseActivity) ctx).saveIntoPrefs(WalletAppConstants.MOBILE, "");
                ((BaseActivity) ctx).saveIntoPrefs(WalletAppConstants.EMAIL, "");
                Intent intent = new Intent(ctx, LoginActivity.class);
                ctx.startActivity(intent);
                ctx.finish();

            }
        });
        CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });
        DialogLogOut.show();
    }


}
