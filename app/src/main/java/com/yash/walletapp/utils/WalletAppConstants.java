package com.yash.walletapp.utils;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by Administrator on 23-Aug-16.
 */
public class WalletAppConstants {

    public static ArrayList<Activity> ACTIVITIES = new ArrayList<>();
    public final static String PREF_NAME = "com.wudstay.prefs";
    public final static String DEFAULT_VALUE = "";
    public final static String EMAIL = "email";
    public final static String USER_ID = "user_id";
    public final static String NAME = "name";
    public final static String MOBILE = "mobile";

}
