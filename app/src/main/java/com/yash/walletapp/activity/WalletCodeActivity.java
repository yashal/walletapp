package com.yash.walletapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.yash.walletapp.R;
import com.yash.walletapp.adapter.SaveCardsAdapter;
import com.yash.walletapp.utils.WalletAppConstants;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class WalletCodeActivity extends BaseActivity implements View.OnClickListener {

    private WalletCodeActivity ctx = this;
    private final static int STORAGE_RESULT = 200;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;
    private ImageView myCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_wallet_code);

        WalletAppConstants.ACTIVITIES.add(ctx);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

         myCode = (ImageView)findViewById(R.id.myCode);
        ImageView share = (ImageView)findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    addPermissionDialogMarshMallow();
                } else {
                    goAhead();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    // add permission dynamically
    private void addPermissionDialogMarshMallow() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode;

        permissions.add(WRITE_EXTERNAL_STORAGE);
        resultCode = STORAGE_RESULT;

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions );
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    if (permissionsToRequest != null && permissionsToRequest.size() > 0)
                        requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_RESULT:
                if (hasPermission(WRITE_EXTERNAL_STORAGE)) {
                    // permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                } else {
                    permissionsRejected.add(WRITE_EXTERNAL_STORAGE);
                    clearMarkAsAsked(WRITE_EXTERNAL_STORAGE);
                    String message = "permission for manage contact was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }
    }
    private void goAhead()
    {
        Bitmap image=((BitmapDrawable)myCode.getDrawable()).getBitmap();
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(ctx.getContentResolver(), image, "Title", null);
        Uri uri  = Uri.parse(path);
        Intent mmsIntent = new Intent(Intent.ACTION_SEND, uri);
        mmsIntent.putExtra("sms_body", "Test");
        mmsIntent.putExtra(Intent.EXTRA_STREAM, uri);
        mmsIntent.setType("image/jpeg");
        startActivity(mmsIntent);
        
    }
}
