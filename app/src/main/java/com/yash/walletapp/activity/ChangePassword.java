package com.yash.walletapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;
import com.yash.walletapp.utils.WalletAppDialogs;


public class ChangePassword extends BaseActivity  {

    private ChangePassword ctx = this;
    private WalletAppDialogs dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_change_password_setting);

        WalletAppConstants.ACTIVITIES.add(ctx);
        dialog = new WalletAppDialogs(ctx);

        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final EditText current_password = (EditText)findViewById(R.id.current_password);
        final EditText new_password = (EditText)findViewById(R.id.new_password);
        final EditText retype_password = (EditText)findViewById(R.id.retype_password);

        LinearLayout save = (LinearLayout)findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str_current_password = current_password.getText().toString().trim();
                String str_new_password = new_password.getText().toString().trim();
                String str_retype_password = retype_password.getText().toString().trim();
                if (str_current_password.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.change_password_failed), getResources().getString(R.string.blank_current));
                }else if (str_new_password.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.change_password_failed), getResources().getString(R.string.blank_new));
                }else if (str_retype_password.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.change_password_failed), getResources().getString(R.string.blank_retype));
                }else if (!str_new_password.equals(str_retype_password)) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.change_password_failed), getResources().getString(R.string.same_new_retype));
                }
                else
                {
                    Toast.makeText(ctx, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        });

        TextView forgot_password = (TextView)findViewById(R.id.forgot_password);
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, ForgotPassword.class);
                startActivity(intent);
            }
        });

    }


}
