package com.yash.walletapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;


public class SecuritySetting extends BaseActivity  {

    private SecuritySetting ctx = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_security_setting);

        WalletAppConstants.ACTIVITIES.add(ctx);

        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        LinearLayout changePassword = (LinearLayout)findViewById(R.id.changePassword);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, ChangePassword.class);
                startActivity(intent);
            }
        });

    }


}
