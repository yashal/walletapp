package com.yash.walletapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;
import com.yash.walletapp.utils.WalletAppDialogs;


public class BankPageActivity extends BaseActivity  {

    private BankPageActivity ctx = this;
    private EditText amount_money, account_holder_name, account_number, ifsc_code, optional;
    private LinearLayout send;
    private WalletAppDialogs dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_page);

        WalletAppConstants.ACTIVITIES.add(ctx);

        dialog = new WalletAppDialogs(ctx);

        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        amount_money = (EditText)findViewById(R.id.amount_money);
        account_holder_name = (EditText)findViewById(R.id.account_holder_name);
        account_number = (EditText)findViewById(R.id.account_number);
        ifsc_code = (EditText)findViewById(R.id.ifsc_code);
        optional = (EditText)findViewById(R.id.optional);

        send = (LinearLayout)findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String money = amount_money.getText().toString().trim();
                String name = account_holder_name.getText().toString().trim();
                String acc_number = account_number.getText().toString().trim();
                String ifsc = ifsc_code.getText().toString().trim();
                String opt = optional.getText().toString().trim();

                if (money.equals(""))
                {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.transaction_failed),
                            getResources().getString(R.string.empty_amount));
                }else if (name.equals(""))
                {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.transaction_failed),
                            getResources().getString(R.string.empty_acc_name));
                }else if (acc_number.equals(""))
                {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.transaction_failed),
                            getResources().getString(R.string.empty_acc_number));
                }else if (ifsc.equals(""))
                {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.transaction_failed),
                            getResources().getString(R.string.empty_ifsc));
                }
                else {
                    Toast.makeText(ctx, "Transfer balance to bank successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

    }

}
