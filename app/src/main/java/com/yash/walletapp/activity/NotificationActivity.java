package com.yash.walletapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.yash.walletapp.R;
import com.yash.walletapp.adapter.NotificationAdapter;
import com.yash.walletapp.utils.WalletAppConstants;


public class NotificationActivity extends BaseActivity implements View.OnClickListener {

    private NotificationActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        WalletAppConstants.ACTIVITIES.add(ctx);
        setDrawerAndToolbar("Notifications");

        recyclerView = (RecyclerView) findViewById(R.id.notification_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter = new NotificationAdapter(ctx);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {

    }
}
