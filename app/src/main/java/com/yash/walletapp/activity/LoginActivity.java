package com.yash.walletapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;
import com.yash.walletapp.utils.WalletAppDialogs;

public class LoginActivity extends BaseActivity {

    private LoginActivity ctx = this;
    private WalletAppDialogs dialog;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        dialog = new WalletAppDialogs(ctx);

        final TextView signup_text = (TextView)findViewById(R.id.signup_text);
        final LinearLayout login = (LinearLayout) findViewById(R.id.login);
        final EditText email_or_mobile = (EditText) findViewById(R.id.mobile);
        final EditText password = (EditText) findViewById(R.id.password);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String email_text = email_or_mobile.getText().toString();
                final String password_text = password.getText().toString();
                if (email_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.login_failed), getResources().getString(R.string.blank_email));
                } else if (!email_text.matches("[0-9]+") && !isValidEmail(email_text)) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.login_failed), getResources().getString(R.string.valid_email));
                } else if (password_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.login_failed), getResources().getString(R.string.blank_password));
                } else {
                    saveIntoPrefs(WalletAppConstants.EMAIL, email_text);
                    saveIntoPrefs(WalletAppConstants.NAME, "");
                    saveIntoPrefs(WalletAppConstants.USER_ID, "");
                    Intent intent = new Intent(ctx, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        signup_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, RegistrationActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
