package com.yash.walletapp.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.yash.walletapp.R;
import com.yash.walletapp.adapter.NotificationAdapter;
import com.yash.walletapp.adapter.SaveCardsAdapter;
import com.yash.walletapp.utils.WalletAppConstants;


public class SaveCardsActivity extends BaseActivity implements View.OnClickListener {

    private SaveCardsActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_save_cards);

        WalletAppConstants.ACTIVITIES.add(ctx);

        recyclerView = (RecyclerView) findViewById(R.id.notification_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter = new SaveCardsAdapter(ctx);
        recyclerView.setAdapter(mAdapter);

        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {

    }
}
