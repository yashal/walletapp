package com.yash.walletapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.widget.Toast;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;

import java.util.ArrayList;

import static android.Manifest.permission.READ_CONTACTS;


public class SplashActivity extends BaseActivity {

    private Handler splashTimeHandler;
    private Runnable finalizer;
    private SplashActivity ctx = this;

    private final static int READ_CONTACT_RESULT = 100;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        // for runtime permission granted
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            addPermissionDialogMarshMallow();
        } else {
            goAhead();
        }

    }

    // add permission dynamically
    private void addPermissionDialogMarshMallow() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode;

        permissions.add(READ_CONTACTS);
        resultCode = READ_CONTACT_RESULT;

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions );
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    if (permissionsToRequest != null && permissionsToRequest.size() > 0)
                        requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case READ_CONTACT_RESULT:
                if (hasPermission(READ_CONTACTS)) {
                    // permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                } else {
                    permissionsRejected.add(READ_CONTACTS);
                    clearMarkAsAsked(READ_CONTACTS);
                    String message = "permission for manage contact was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }
    }
    private void goAhead()
    {
        splashTimeHandler = new Handler();
        finalizer = new Runnable() {
            public void run() {
                if (getFromPrefs(WalletAppConstants.EMAIL).equals("")) {
                    Intent mainIntent = new Intent(ctx, LoginActivity.class);
                    startActivity(mainIntent);
                } else {
                    Intent mainIntent = new Intent(ctx, HomeActivity.class);
                    startActivity(mainIntent);
                }
                finish();
            }
        };
        splashTimeHandler.postDelayed(finalizer, 1000);
    }
}
