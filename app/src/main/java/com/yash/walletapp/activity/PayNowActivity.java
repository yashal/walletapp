package com.yash.walletapp.activity;

import android.content.Intent;
import android.database.Cursor;
import android.media.Image;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;
import com.yash.walletapp.utils.WalletAppDialogs;

public class PayNowActivity extends BaseActivity {

    private PayNowActivity ctx = this;
    private static final int CONTACT_PICKER_RESULT = 1001;
    private static final String DEBUG_TAG = "Contact List";
    private static final int RESULT_OK = -1;
    private EditText mobile_no_pay;
    private String mobilePhone;
    private WalletAppDialogs dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_now);

        WalletAppConstants.ACTIVITIES.add(ctx);

        dialog = new WalletAppDialogs(ctx);
        mobile_no_pay = (EditText) findViewById(R.id.mobile_no_pay);
        if (getIntent().getStringExtra("mobile_no") != null)
        {
            mobilePhone = getIntent().getStringExtra("mobile_no");
            mobile_no_pay.setText(mobilePhone);
        }
        final EditText amount = (EditText)findViewById(R.id.amount);
        final EditText optional = (EditText)findViewById(R.id.optional);

        final ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, PayActivity.class);
                startActivity(intent);
                finish();
            }
        });

        final LinearLayout send = (LinearLayout)findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = mobile_no_pay.getText().toString().trim();
                String _amount = amount.getText().toString().trim();
                if (phone.equals("") && !phone.matches("[0-9]+"))
                {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.payment_failed), getResources().getString(R.string.valid_mobile_reg));
                }else if (_amount.equals(""))
                {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.payment_failed), getResources().getString(R.string.blank_amount));
                }
                else
                {
                    Toast.makeText(ctx, "Your amount "+_amount+" is sent to "+phone+" successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

        mobile_no_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (mobile_no_pay.getRight() - mobile_no_pay.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        System.out.println("Yash Bhardwaj");
                        Intent it = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                        startActivityForResult(it, CONTACT_PICKER_RESULT);

                        return true;
                    }
                }
                return false;
            }
        });

    }

    // handle after selecting a contact from the list
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
                    getContactInfo(data, mobile_no_pay);
                    break;
            }
        } else {
            // gracefully handle failure
            Log.w(DEBUG_TAG, "Warning: activity result not ok");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ctx, PayActivity.class);
        startActivity(intent);
        finish();
    }
}
