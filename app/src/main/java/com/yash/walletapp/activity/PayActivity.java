package com.yash.walletapp.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;
import com.yash.walletapp.utils.WalletAppDialogs;

import java.util.ArrayList;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;

public class PayActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    private PayActivity ctx = this;
    private static final int CONTACT_PICKER_RESULT = 1001;
    private static final String DEBUG_TAG = "Contact List";
    private static final int RESULT_OK = -1;
    private EditText mobile_no_pay;
    private String phoneNumber;
    private Handler handler;
    private Runnable finalizer;
    private WalletAppDialogs dialog;
    private ZXingScannerView  mScannerView;
    private final static int CAMERA_RESULT = 101;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        setDrawerAndToolbar("Enter Mobile No to Pay");
        WalletAppConstants.ACTIVITIES.add(ctx);
        dialog = new WalletAppDialogs(ctx);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mScannerView = (ZXingScannerView) findViewById(R.id.qrcode_scan);
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            addPermissionDialogMarshMallow();
        } else {
            QrScanner();
        }
*/
        mobile_no_pay = (EditText) findViewById(R.id.mobile_no_pay);
        mobile_no_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToPaynow();
            }
        });
        mobile_no_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (mobile_no_pay.getRight() - mobile_no_pay.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        System.out.println("Yash Bhardwaj");
                        Intent it = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                        startActivityForResult(it, CONTACT_PICKER_RESULT);

                        return true;
                    }
                }
                return false;
            }
        });


    }

    // handle after selecting a contact from the list
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
                    getContactInfo(data, mobile_no_pay);
                     phoneNumber = mobile_no_pay.getText().toString().trim();
                    if (phoneNumber.equals("") && !phoneNumber.matches("[0-9]+"))
                    {
                        dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.payment_failed), getResources().getString(R.string.valid_mobile_reg));
                    }
                    else
                    {
                        handler = new Handler();
                        finalizer = new Runnable() {
                            public void run() {
                                moveToPaynow();
                            }
                        };
                        handler.postDelayed(finalizer, 600);
                    }
                    break;
            }
        } else {
            // gracefully handle failure
            Log.w(DEBUG_TAG, "Warning: activity result not ok");
        }
    }

    public void moveToPaynow() {
        Intent intent = new Intent(ctx, PayNowActivity.class);
        intent.putExtra("mobile_no", phoneNumber);
        startActivity(intent);
        finish();
    }

    @Override
    public void handleResult(Result result) {
        Log.e("handler", result.getText()); // Prints scan results
        Log.e("handler", result.getBarcodeFormat().toString()); // Prints the scan format (qrcode)

        // show the scanner result into dialog box.
        phoneNumber = result.getText().toString();
        mScannerView.resumeCameraPreview(this);
        moveToPaynow();
    }

    public void QrScanner(){
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            addPermissionDialogMarshMallow();
        } else {
            QrScanner();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    // add permission dynamically
    private void addPermissionDialogMarshMallow() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode;

        permissions.add(CAMERA);
        resultCode = CAMERA_RESULT;

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions );
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    QrScanner();
                }

                if (permissionsRejected.size() > 0) {

                    if (permissionsToRequest != null && permissionsToRequest.size() > 0)
                        requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case CAMERA_RESULT:
                if (hasPermission(CAMERA)) {
                    // permissionSuccess.setVisibility(View.VISIBLE);
                    QrScanner();
                } else {
                    permissionsRejected.add(CAMERA);
                    clearMarkAsAsked(CAMERA);
                    String message = "permission for manage Camera was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }
    }
}
