package com.yash.walletapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;


public class SendMoneyBankActivity extends BaseActivity  {

    private SendMoneyBankActivity ctx = this;
    private LinearLayout transfer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_transfer_bank);

        WalletAppConstants.ACTIVITIES.add(ctx);

        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        transfer = (LinearLayout)findViewById(R.id.transfer);

        transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(ctx, BankPageActivity.class);
                startActivity(mIntent);
            }
        });

    }
}
