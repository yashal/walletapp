package com.yash.walletapp.activity;

import android.os.Bundle;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;

/**
 * Created by Yash on 2/9/2017.
 */

public class AcceptMyWalletActivity extends BaseActivity {

    private AcceptMyWalletActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_accept_mywallet );
        setDrawerAndToolbar("Accept My Wallet");

        WalletAppConstants.ACTIVITIES.add(ctx);
    }
}
