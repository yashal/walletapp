package com.yash.walletapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yash.walletapp.R;
import com.yash.walletapp.adapter.PassbooktAdapter;
import com.yash.walletapp.utils.WalletAppConstants;

public class PassBookActivity extends BaseActivity implements View.OnClickListener{

    private PassBookActivity ctx = this;
    private LinearLayout tab1_layout, tab2_layout, tab3_layout, tab4_layout,tab1_selector, tab2_selector,tab3_selector, tab4_selector;
    private TextView tab1_text, tab2_text, tab3_text, tab4_text;
    private ViewPager passbook__pager;
    private PassbooktAdapter mAdapter;
    private LinearLayout add_money_LL, send_bank_LL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_passbook);

        setDrawerAndToolbar("Passbook");
        WalletAppConstants.ACTIVITIES.add(ctx);

        add_money_LL = (LinearLayout)findViewById(R.id.add_money_LL);
        send_bank_LL = (LinearLayout)findViewById(R.id.send_bank_LL);

        tab1_layout = (LinearLayout)findViewById(R.id.tab1_layout);
        tab2_layout = (LinearLayout)findViewById(R.id.tab2_layout);
        tab3_layout = (LinearLayout)findViewById(R.id.tab3_layout);
        tab4_layout = (LinearLayout)findViewById(R.id.tab4_layout);

        tab1_selector = (LinearLayout)findViewById(R.id.tab1_selector);
        tab2_selector = (LinearLayout)findViewById(R.id.tab2_selector);
        tab3_selector = (LinearLayout)findViewById(R.id.tab3_selector);
        tab4_selector = (LinearLayout)findViewById(R.id.tab4_selector);

        tab1_text = (TextView)findViewById(R.id.tab1_text);
        tab2_text = (TextView)findViewById(R.id.tab2_text);
        tab3_text = (TextView)findViewById(R.id.tab3_text);
        tab4_text = (TextView)findViewById(R.id.tab4_text);

        mAdapter = new PassbooktAdapter(getSupportFragmentManager());
        passbook__pager = (ViewPager) findViewById(R.id.passbook__pager);
        passbook__pager.setAdapter(mAdapter);
        passbook__pager.setOffscreenPageLimit(3);

        tab1_layout.setOnClickListener(this);
        tab2_layout.setOnClickListener(this);
        tab3_layout.setOnClickListener(this);
        tab4_layout.setOnClickListener(this);

        setStyle(0);

        passbook__pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                setStyle(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        add_money_LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(ctx, AddMoneyActivity.class);
                startActivity(mIntent);
            }
        });

        send_bank_LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(ctx, SendMoneyBankActivity.class);
                startActivity(mIntent);
            }
        });


    }

    private void setStyle(int position) {
        if (position == 0) {
            tab1_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
            tab2_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            tab3_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            tab4_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));

            tab1_selector.setVisibility(View.VISIBLE);
            tab2_selector.setVisibility(View.GONE);
            tab3_selector.setVisibility(View.GONE);
            tab4_selector.setVisibility(View.GONE);
        } else if (position == 1) {
            tab1_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            tab2_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
            tab3_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            tab4_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));

            tab1_selector.setVisibility(View.GONE);
            tab2_selector.setVisibility(View.VISIBLE);
            tab3_selector.setVisibility(View.GONE);
            tab4_selector.setVisibility(View.GONE);
        } else if (position == 2) {
            tab1_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            tab2_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            tab3_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
            tab4_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));

            tab1_selector.setVisibility(View.GONE);
            tab2_selector.setVisibility(View.GONE);
            tab3_selector.setVisibility(View.VISIBLE);
            tab4_selector.setVisibility(View.GONE);
        } else if (position == 3) {
            tab1_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            tab2_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            tab3_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            tab4_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));

            tab1_selector.setVisibility(View.GONE);
            tab2_selector.setVisibility(View.GONE);
            tab3_selector.setVisibility(View.GONE);
            tab4_selector.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab1_layout:
                if (passbook__pager.getCurrentItem() != 0) {
                    passbook__pager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.tab2_layout:
                if (passbook__pager.getCurrentItem() != 1) {
                    passbook__pager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            case R.id.tab3_layout:
                if (passbook__pager.getCurrentItem() != 2) {
                    passbook__pager.setCurrentItem(2);
                    setStyle(2);
                }
                break;

            case R.id.tab4_layout:
                if (passbook__pager.getCurrentItem() != 3) {
                    passbook__pager.setCurrentItem(3);
                    setStyle(3);
                }
                break;
        }
    }
}
