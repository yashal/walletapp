package com.yash.walletapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;
import com.yash.walletapp.utils.WalletAppDialogs;

public class RegistrationActivity extends BaseActivity {

    private RegistrationActivity ctx = this;
    private WalletAppDialogs dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        final ImageView back = (ImageView)findViewById(R.id.back);
        final EditText mobile = (EditText)findViewById(R.id.mobile);
        final EditText email = (EditText)findViewById(R.id.email);
        final EditText password = (EditText)findViewById(R.id.password);
        final EditText password_confirm = (EditText)findViewById(R.id.password_confirm);
        final LinearLayout save = (LinearLayout)findViewById(R.id.save);

        dialog = new WalletAppDialogs(ctx);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String mobile_text = mobile.getText().toString();
                final String email_text = email.getText().toString();
                final String password_text = password.getText().toString();
                final String confirm_password_text = password_confirm.getText().toString();
                if (mobile_text.equals("") && !mobile_text.matches("[0-9]+")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.valid_mobile_reg));
                }else if (email_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.blank_email_reg));
                }else if (!isValidEmail(email_text)) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.valid_email_reg));
                }else if (password_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.blank_password));
                }else if (confirm_password_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.blank_confirm_password));
                }else if (!confirm_password_text.equals(password_text)) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.mismatch_password));
                }
                else
                {
                    saveIntoPrefs(WalletAppConstants.EMAIL, email_text);
                    saveIntoPrefs(WalletAppConstants.MOBILE, mobile_text);
                    saveIntoPrefs(WalletAppConstants.NAME, "");
                    saveIntoPrefs(WalletAppConstants.USER_ID, "");
                    Intent intent = new Intent(ctx, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ctx, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
