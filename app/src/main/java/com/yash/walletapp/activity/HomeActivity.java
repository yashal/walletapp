package com.yash.walletapp.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;

public class HomeActivity extends BaseActivity {

    private HomeActivity ctx = this;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setDrawerAndToolbar("Home");
        WalletAppConstants.ACTIVITIES.add(ctx);

        LinearLayout savedcards = (LinearLayout)findViewById(R.id.savedcards);
        savedcards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, SaveCardsActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout myWalletCode = (LinearLayout)findViewById(R.id.myWalletCode);
        myWalletCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, WalletCodeActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout notificationSettings = (LinearLayout)findViewById(R.id.notificationSettings);
        notificationSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, NotificationSettingsActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout securitySettings = (LinearLayout)findViewById(R.id.securitySettings);
        securitySettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, SecuritySetting.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
