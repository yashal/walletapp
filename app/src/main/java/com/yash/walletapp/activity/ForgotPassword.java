package com.yash.walletapp.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yash.walletapp.R;
import com.yash.walletapp.utils.WalletAppConstants;
import com.yash.walletapp.utils.WalletAppDialogs;


public class ForgotPassword extends BaseActivity  {

    private ForgotPassword ctx = this;
    private WalletAppDialogs dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_forgot_password);

        WalletAppConstants.ACTIVITIES.add(ctx);
        dialog = new WalletAppDialogs(ctx);

        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        final EditText mobile = (EditText)findViewById(R.id.mobile);
        if (!getFromPrefs(WalletAppConstants.MOBILE).equals(""))
        {
            mobile.setText(getFromPrefs(WalletAppConstants.MOBILE));
        }else if (!getFromPrefs(WalletAppConstants.EMAIL).equals(""))
        {
            mobile.setText(getFromPrefs(WalletAppConstants.EMAIL));
        }

        LinearLayout proceed = (LinearLayout)findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mobile.getText().toString().equals(""))
                {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.change_password_failed), getResources().getString(R.string.blank_email));
                }
                else {
                    dialog.displayCommonDialogWithHeaderSmall("Alert", getResources().getString(R.string.forgot_password_dialog_text));
                    finish();
                }
            }
        });

    }


}
